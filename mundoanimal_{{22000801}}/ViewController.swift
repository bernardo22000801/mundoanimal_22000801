//
//  ViewController.swift
//  mundoanimal_{{22000801}}
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Alamofire
import Kingfisher
struct Animal: Decodable {
    let name: String
    let status: String
}

class ViewController: UIViewController {
    
    @IBOutlet weak var image_link: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getNovoAnimal()
        
    }
    func getNovoAnimal() {
        AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of: Animal.self) {response in
            if let animal = response.value {
                self.image_link.kf.setImage(with: URL(string: animal.name))
                print(animal.name)
            }
        }
    }
    
}

